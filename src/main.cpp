
#include "backend.h"
#include "device.h"
#include "tensor_keeper.h"
#include "tensor.h"
#include "initializers.h"
// #include "layer.h"

#include <stdio.h>
#include <string.h>
#include <unordered_map>

#include "error_utils.h"

#include <functional>
#include <float.h>


inline float epsilon() {
	return 1e-2f;
}


struct base_class {
    virtual void temp() = 0;
};

struct derived1 : base_class {
    void temp() {}
};

std::unordered_map<int, base_class*> values;

bool test_const_initializer(dnn::TensorKeeper* keeper, dnn::Device* device) {
	dnn::RandomGenerator* generator = dnn::initializers::create_generator(device, 1);
	dnn::initializers::set_seed(generator, 10);

	dnn::Tensor tensor = dnn::tensor_keeper::create_tensor(keeper, device, 16, 255, 255, 3);
	dnn::tensor_keeper::to_cpu(keeper, tensor);
	dnn::TensorDescriptor* descriptor = dnn::tensor_keeper::get_descriptor(keeper, tensor);

	float val = 9.5f;
	dnn::initializers::constant(generator, descriptor, val);
	/// dnn::initializers::uniform_rand(generator, descriptor, 10.0f, 100.0f);
	for (int i = 0; i < descriptor->count; i++) {
		if (descriptor->cpu_data[i] != val)
			return false;
	}

	/// cleanup 
	dnn::tensor_keeper::destroy_tensor(keeper, tensor);
	dnn::initializers::destroy_generator(generator);

	return true;;
}

bool test_fc(dnn::TensorKeeper* keeper, dnn::BackendCUDNN* backend, dnn::Device* device) {
	dnn::RandomGenerator* generator = dnn::initializers::create_generator(device, 1);
	dnn::initializers::set_seed(generator, 10);

	/// Forward pass test
	dnn::Tensor input_tensor = dnn::tensor_keeper::create_tensor(keeper, device, 2, 4, 2, 1);
	dnn::TensorDescriptor* input_descriptor = dnn::tensor_keeper::get_descriptor(keeper, input_tensor);
	float val = 9.5f;
	dnn::initializers::constant(generator, input_descriptor, val);
	
	/// Can put these ops in an array 
	dnn::Operation op = dnn::backend_cudnn::create_fully_connected(backend, input_tensor, 2, false);
	dnn::CUDNNLinearDescriptors* op_descriptor = dnn::backend_cudnn::get_operation_descriptor<dnn::CUDNNLinearDescriptors>(backend, op);

	dnn::TensorDescriptor* params_descriptor = dnn::tensor_keeper::get_descriptor(keeper, op_descriptor->params);
	val = 1.0f;
	dnn::initializers::constant(generator, params_descriptor, val);

	dnn::Tensor output_tensor = dnn::backend_cudnn::forward_fully_connected(backend, input_tensor, op);
	dnn::tensor_keeper::to_cpu(keeper, output_tensor);

	dnn::TensorDescriptor* ouput_descriptor = dnn::tensor_keeper::get_descriptor(keeper, output_tensor);
	for (int i = 0; i < ouput_descriptor->count; i++) {
		if (ouput_descriptor->cpu_data[i] != 76.0f)
			return false;
	}


	/// Backward pass test
	dnn::initializers::uniform_rand(generator, input_descriptor, 0.0f, 1.0f);

	dnn::initializers::uniform_rand(generator, params_descriptor, 0.0f, 1.0f);
	dnn::tensor_keeper::to_cpu(keeper, op_descriptor->params);
	for (int i = 0; i < params_descriptor->count; i++)
		printf("param %f \n", params_descriptor->cpu_data[i]);


	dnn::tensor_keeper::to_cpu(keeper, input_tensor);
	for (int i = 0; i < input_descriptor->count; i++)
		printf("input %f \n", input_descriptor->cpu_data[i]);

	/// Analytical gradient
	output_tensor = dnn::backend_cudnn::forward_fully_connected(backend, input_tensor, op);
	dnn::tensor_keeper::to_cpu(keeper, output_tensor);
	for (int i = 0; i < ouput_descriptor->count; i++)
		printf("output %f \n", ouput_descriptor->cpu_data[i]);


	dnn::Tensor output_grad = dnn::tensor_keeper::duplicate_tensor(keeper, output_tensor, true);
	dnn::TensorDescriptor* output_grad_descriptor = dnn::tensor_keeper::get_descriptor(keeper, output_grad);
	dnn::tensor_keeper::to_cpu(keeper, output_grad);
	for (int i = 0; i < output_grad_descriptor->count; i++)
		output_grad_descriptor->cpu_data[i] = 0.0;
	output_grad_descriptor->cpu_data[0] = 1.0f;
	dnn::tensor_keeper::to_cuda(keeper, output_grad);
	//val = 0.0f;
	//dnn::initializers::constant(generator, output_grad_descriptor, val);
	dnn::Tensor input_analytical_grad = dnn::backend_cudnn::backward_fully_connected(backend, output_grad, op);

	dnn::tensor_keeper::to_cpu(keeper, input_analytical_grad);
	dnn::TensorDescriptor* in_1_descriptor = dnn::tensor_keeper::get_descriptor(keeper, input_analytical_grad);
	for (int i = 0; i < in_1_descriptor->count; i++)
		printf("input gradients %f \n", in_1_descriptor->cpu_data[i]);
	float in_1_grad = in_1_descriptor->cpu_data[0];

	/// Numerical gradient
	float h = sqrt(FLT_EPSILON);

	dnn::Tensor inc_input = dnn::tensor_keeper::duplicate_tensor(keeper, input_tensor, true);
	dnn::TensorDescriptor* inc_input_desc = dnn::tensor_keeper::get_descriptor(keeper, inc_input);

	dnn::tensor_keeper::to_cpu(keeper, inc_input);
	inc_input_desc->cpu_data[0] += h;

	dnn::tensor_keeper::to_gpu_memsync(inc_input_desc);

	dnn::Tensor dec_input = dnn::tensor_keeper::duplicate_tensor(keeper, input_tensor, true);
	dnn::TensorDescriptor* dec_input_desc = dnn::tensor_keeper::get_descriptor(keeper, dec_input);

	dnn::tensor_keeper::to_cpu(keeper, dec_input);
	dec_input_desc->cpu_data[0] -= h;
	dnn::tensor_keeper::to_gpu_memsync(dec_input_desc);

	dnn::backend_cudnn::forward_fully_connected(backend, inc_input, op);
	dnn::tensor_keeper::to_cpu_memsync(ouput_descriptor);
	for (int i = 0; i < ouput_descriptor->count; i++)
		printf("output 1 %f \n", ouput_descriptor->cpu_data[i]);
	float out_1 = ouput_descriptor->cpu_data[0];

	dnn::backend_cudnn::forward_fully_connected(backend, dec_input, op);
	dnn::tensor_keeper::to_cpu_memsync(ouput_descriptor);
	for (int i = 0; i < ouput_descriptor->count; i++)
		printf("output 2 %f \n", ouput_descriptor->cpu_data[i]);
	float out_2 = ouput_descriptor->cpu_data[0];

	float in_2_grad = (out_1 - out_2) / (2 * h);

	if (abs(in_2_grad - in_1_grad) < epsilon()) {
		printf("fully connected gradient test passed with error margin %.9g \n", in_2_grad - in_1_grad);
	}

	printf("numerical %.9g analytical %.9g \n", in_2_grad, in_1_grad);
	/// Cleanup
	dnn::tensor_keeper::destroy_tensor(keeper, input_tensor);
	dnn::backend_cudnn::destroy_fully_connected(backend, op);
	dnn::initializers::destroy_generator(generator);
	return true;
}

bool test_fc_gradeint() { return false;  }

int main(int argc, char** argv) {
    values[0] = new derived1();
    values[2] = new derived1();

	int digs = DECIMAL_DIG;
	printf("%.*e \n", digs, sqrt(FLT_EPSILON));
	printf("%.*e \n", digs, FLT_EPSILON);

	printf("%.9g \n", sqrt(FLT_EPSILON));
	printf("%.9g \n", FLT_EPSILON);
	//FLT_EPSILON


	int device_id;
	cudaGetDevice(&device_id);
	printf(" Device index %d", device_id);

	check_cuda(cudaSetDevice(device_id));

	dnn::Device device;
	device.device_id = device_id;
	device.type = dnn::DeviceType::GPU;

	dnn::TensorKeeper keeper = dnn::tensor_keeper::create();
	
	if (!test_const_initializer(&keeper, &device)) {
		printf("Constant initialization test failed \n");
	}
	check_cuda(cudaDeviceSynchronize());
	
	
	dnn::BackendCUDNN backend;
	if (dnn::backend_cudnn::create_backend(backend, &keeper, &device)) {
		/// Call Test functions here
		if (!test_fc(&keeper, &backend, &device)) {
			printf(" FC test failed \n");
		}
	}

	check_cuda(cudaDeviceSynchronize());

	/// Cleanup
	dnn::backend_cudnn::destroy_backend(backend);
	dnn::tensor_keeper::destroy();

	check_cuda(cudaDeviceReset());

}

/// ------- Device ------ ///
#include <unordered_map>
#include <vector>

/// Might be unnecessary
struct BackwardPassResourceManager {
    std::unordered_map<int, int> input_delta;
    std::unordered_map<int, int> params_delta;
};

struct Optimizer {
    std::vector<std::pair<int,int>> params_w_gradients;
};