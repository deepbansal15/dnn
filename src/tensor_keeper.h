#pragma once

#include "defines.h"
#include "tensor.h"

BEGIN_NAMESPACE

struct Device;

namespace tensor_keeper {

	void create();
	Tensor create_tensor(Device& device, TensorShape& shape);
	TensorDesc& get_descriptor(Tensor t);
	void allocate_cpu_tensor(TensorDesc& descriptor);
	void allocate_gpu_tensor(TensorDesc& descriptor);
	Tensor duplicate_tensor(Tensor tensor, bool copy);
	void destroy_tensor(Tensor tensor);
	bool get_descriptor(TensorDesc& desc, Tensor tensor);
	Tensor to_cuda(Tensor tensor);
	Tensor to_cpu(Tensor tensor);

	void destroy();
	void get_total_memory(Device* device);

}

END_NAMESPACE