#pragma once

#include "defines.h"

BEGIN_NAMESPACE

struct Operation {
    int handle;
};

/// TODO : Remove gradients from here
struct OpDesc {
    Tensor input = -1;
    Tensor output = -1;
    Tensor filter = -1;
    Tensor bias = -1;

    Tensor dx = -1;
    Tensor dy = -1;
    Tensor dw = -1;
    Tensor db = -1;
};

struct ConvParam {
    int pad_h;
    int pad_w;
    int stride_h;
    int stride_w;
    int dilation_h;
    int dilation_w;
    int filter_h;
    int filter_w;
    int filter_k;
};

struct LinearParam {

};

END_NAMESPACE