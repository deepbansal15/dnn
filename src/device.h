#pragma once

#include "defines.h"

BEGIN_NAMESPACE

enum class DeviceType {
    CPU,
    GPU
};

struct Device {
    // Implicit Constructor
    Device(DeviceType type, int index = -1)
        : type(type), index(index) {}
    
    DeviceType type;
    int index;
};

END_NAMESPACE