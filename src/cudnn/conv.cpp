#include "backend_cudnn.h"
#include <tensor_keeper.h>
#include <Device.h>

/// TODO : micro-batches & group convolution
/// TODO : Idea 1) Can pass the whole backend containing keeper, cudnnhandle, 
///                 containing everything required for current and future use
///             2) Separate out backend data fwd,bwd etc. from function, 
///                 backend should not be fixed but depend upon the op 
///                 example keeping a global backend variable
/// TODO : Fix naming convention ex- use TensorDetail instead of TensorDescriptor
BEGIN_NAMESPACE

struct CUDNNConvDesc : OpDesc {
    // Forward and backward pass uses same descriptors
    cudnnTensorDescriptor_t xdesc;
    cudnnTensorDescriptor_t ydesc;
    cudnnFilterDescriptor_t wdesc;

    cudnnConvolutionDescriptor_t conv_desc;

    cudnnConvolutionFwdAlgo_t fwd_algo;
    cudnnConvolutionBwdDataAlgo_t bwd_data_algo;
    cudnnConvolutionBwdFilterAlgo_t bwd_filter_algo;

    Tensor workspace = -1;
    size_t workspace_size;
};

CUDNNConvDesc* create_conv2d(ConvParam& param, TensorShape& x_dim) {
    CUDNNConvDesc* conv_desc = new CUDNNConvDesc();
      
    cudnnCreateTensorDescriptor(&conv_desc->xdesc);
    cudnnSetTensor4dDescriptor(conv_desc->xdesc,
        cudnnTensorFormat_t::CUDNN_TENSOR_NCHW,
        cudnnDataType_t::CUDNN_DATA_FLOAT,
        x_dim.n,
        x_dim.c,
        x_dim.h,
        x_dim.w
    );
    
    TensorShape filter_dim = { 
        n : param.filter_k, 
        c : x_dim.c, 
        h : param.filter_h, 
        w : param.filter_w 
    };

    cudnnCreateFilterDescriptor(&conv_desc->wdesc);
    cudnnSetFilter4dDescriptor(conv_desc->wdesc,
        cudnnDataType_t::CUDNN_DATA_FLOAT, 
        cudnnTensorFormat_t::CUDNN_TENSOR_NCHW,
        param.filter_k, 
        x_dim.c, 
        param.filter_h, 
        param.filter_w
    );
    
    cudnnCreateConvolutionDescriptor(&conv_desc->conv_desc);
    cudnnSetConvolution2dDescriptor(conv_desc->conv_desc, 
        param.pad_h, 
        param.pad_w, 
        param.stride_h, 
        param.stride_w, 
        param.dilation_h,
        param.dilation_w, 
        cudnnConvolutionMode_t::CUDNN_CROSS_CORRELATION,
        cudnnDataType_t::CUDNN_DATA_FLOAT
    );
    
    TensorShape y_dim;
    cudnnGetConvolution2dForwardOutputDim(conv_desc->conv_desc,
        conv_desc->xdesc, 
        conv_desc->wdesc,
        &y_dim.n, 
        &y_dim.c, 
        &y_dim.h, 
        &y_dim.w
    );
    
    /// Allocate the tensor from tensorkeeper
    cudnnCreateTensorDescriptor(&conv_desc->ydesc);
    cudnnSetTensor4dDescriptor(conv_desc->ydesc,
        cudnnTensorFormat_t::CUDNN_TENSOR_NCHW,
        cudnnDataType_t::CUDNN_DATA_FLOAT,
        y_dim.n,
        y_dim.c,
        y_dim.h,
        y_dim.w
    );
    
    // TensorKeeper passing without singleton 
    // Remove tensorkeeper dependicies
    Device device = DeviceType::GPU;
    conv_desc->filter = tensor_keeper::create_tensor(device, filter_dim); 
    conv_desc->output = tensor_keeper::create_tensor(device, y_dim);
    // Create tensor for backward pass
    conv_desc->dx = tensor_keeper::create_tensor(device, x_dim);
    conv_desc->dw = tensor_keeper::create_tensor(device, filter_dim); 

	return conv_desc;
}

bool get_conv_algos(cudnnHandle_t handle, CUDNNConvDesc& conv_desc) {
    cudnnGetConvolutionForwardAlgorithm(handle, conv_desc.xdesc,
        conv_desc.wdesc, conv_desc.conv_desc, 
        conv_desc.ydesc, CUDNN_CONVOLUTION_FWD_PREFER_FASTEST,
        0, &conv_desc.fwd_algo
    );

    cudnnGetConvolutionBackwardDataAlgorithm(handle,
        conv_desc.wdesc, conv_desc.ydesc,
        conv_desc.conv_desc, conv_desc.xdesc,
        CUDNN_CONVOLUTION_BWD_DATA_PREFER_FASTEST, 0,
        &conv_desc.bwd_data_algo
    );
            
    cudnnGetConvolutionBackwardFilterAlgorithm(handle, 
        conv_desc.xdesc, conv_desc.ydesc, 
        conv_desc.conv_desc, conv_desc.wdesc,
        CUDNN_CONVOLUTION_BWD_FILTER_PREFER_FASTEST, 0,
        &conv_desc.bwd_filter_algo
    );
}

bool get_conv_wrk_size(cudnnHandle_t handle, CUDNNConvDesc& conv_desc) {
    size_t wrk_size = 0;
    size_t max_wrk_size = 0;
    
    cudnnGetConvolutionForwardWorkspaceSize(handle, conv_desc.xdesc,
        conv_desc.wdesc, conv_desc.conv_desc, 
        conv_desc.ydesc, conv_desc.fwd_algo, &wrk_size
    );
    max_wrk_size = wrk_size > max_wrk_size ? wrk_size : max_wrk_size;

    cudnnGetConvolutionBackwardDataWorkspaceSize(handle, conv_desc.wdesc,
        conv_desc.ydesc, conv_desc.conv_desc, 
        conv_desc.xdesc, conv_desc.bwd_data_algo, &wrk_size
    );
    max_wrk_size = wrk_size > max_wrk_size ? wrk_size : max_wrk_size;

    cudnnGetConvolutionBackwardFilterWorkspaceSize(handle, conv_desc.xdesc,
        conv_desc.ydesc, conv_desc.conv_desc, 
        conv_desc.wdesc, conv_desc.bwd_filter_algo, &wrk_size
    );
    max_wrk_size = wrk_size > max_wrk_size ? wrk_size : max_wrk_size;

    conv_desc.workspace_size = max_wrk_size;
}

bool destroy_conv(void* op_desc) {
    CUDNNConvDesc* cdesc = static_cast<CUDNNConvDesc*>(op_desc);

    cudnnDestroyFilterDescriptor(cdesc->wdesc);
    cudnnDestroyTensorDescriptor(cdesc->xdesc);
    cudnnDestroyTensorDescriptor(cdesc->ydesc);

    cudnnDestroyConvolutionDescriptor(cdesc->conv_desc);

    tensor_keeper::destroy_tensor(cdesc->workspace);

    tensor_keeper::destroy_tensor(cdesc->filter);
    tensor_keeper::destroy_tensor(cdesc->output);

    tensor_keeper::destroy_tensor(cdesc->dx);
    tensor_keeper::destroy_tensor(cdesc->dw);
}

Tensor fwd(cudnnHandle_t handle, void* op_desc, Tensor input) {
    CUDNNConvDesc* cdesc = static_cast<CUDNNConvDesc*>(op_desc);

    float alpha = 1;
    float beta = 0;
    TensorDesc xdesc = tensor_keeper::get_descriptor(cdesc->input);
    TensorDesc ydesc = tensor_keeper::get_descriptor(cdesc->output);
    TensorDesc wdesc = tensor_keeper::get_descriptor(cdesc->filter);
    TensorDesc wrk_desc = tensor_keeper::get_descriptor(cdesc->workspace);

    cudnnConvolutionForward(handle, &alpha, cdesc->xdesc,
        xdesc.gpu_data, cdesc->wdesc, wdesc.gpu_data, cdesc->conv_desc, 
        cdesc->fwd_algo, wrk_desc.gpu_data, cdesc->workspace_size, 
        &beta, cdesc->ydesc, ydesc.gpu_data
    );

    return cdesc->output;

}

Tensor bwd(cudnnHandle_t handle, void* op_desc, Tensor dy) {
    CUDNNConvDesc* cdesc = static_cast<CUDNNConvDesc*>(op_desc);

    float alpha = 1, beta = 0;

    TensorDesc xdesc = tensor_keeper::get_descriptor(cdesc->input);
    TensorDesc dxdesc = tensor_keeper::get_descriptor(cdesc->dx);
    TensorDesc dydesc = tensor_keeper::get_descriptor(dy);
    TensorDesc wdesc = tensor_keeper::get_descriptor(cdesc->filter);
    TensorDesc dwdesc = tensor_keeper::get_descriptor(cdesc->dw);
    TensorDesc wrk_desc = tensor_keeper::get_descriptor(cdesc->workspace);


    cudnnConvolutionBackwardData(handle, &alpha,
        cdesc->wdesc, wdesc.gpu_data, cdesc->ydesc, dydesc.gpu_data, 
        cdesc->conv_desc, cdesc->bwd_data_algo, wrk_desc.gpu_data,
        cdesc->workspace_size, &beta, cdesc->xdesc, dxdesc.gpu_data
    );

    cudnnConvolutionBackwardFilter(handle, &alpha, 
        cdesc->xdesc, xdesc.gpu_data, cdesc->ydesc, 
        dydesc.gpu_data, cdesc->conv_desc, cdesc->bwd_filter_algo, 
        wrk_desc.gpu_data, cdesc->workspace_size, &beta,
        cdesc->wdesc, dwdesc.gpu_data
    );

    return cdesc->dx;
}

CUDNNOpDetail* BackendCUDNN::get_conv_detail(ConvParam& args, TensorShape& x_dim) {
    CUDNNConvDesc* conv_desc = create_conv2d(args, x_dim);
    get_conv_algos(handle, *conv_desc);
    get_conv_wrk_size(handle, *conv_desc);

}

END_NAMESPACE