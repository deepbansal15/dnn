#include <defines.h>

#include <cudnn.h>

BEGIN_NAMESPACE

namespace cudnn {
    cudnnHandle_t get_handle();
    void destroy_handle();
}

END_NAMESPACE