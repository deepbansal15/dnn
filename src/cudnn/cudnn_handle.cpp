#include "cudnn_handle.h"

#include <cuda/cuda_utils.h>
#include <stdio.h>

BEGIN_NAMESPACE

namespace cudnn {
    cudnnHandle_t cudnn_handle;
    
    void create_cublas_handle() {
        check_cudnn(cudnnCreate(&cudnn_handle));
    }

    void destroy_cublas_handle() {
        check_cudnn(cudnnDestroy(cudnn_handle));
    }

    cudnnHandle_t get_cublas_handle() {
        if (!cudnn_handle) {
            create_cublas_handle();
        }

        return cudnn_handle;
    }
}

END_NAMESPACE