#pragma once

#include <backend_types.h>
#include <tensor.h>

#include <cublas_v2.h>
#include <cudnn.h>
#include <unordered_map>

BEGIN_NAMESPACE

struct TensorKeeper;

template< typename T>
struct OpDetail {};

template<>
struct OpDetail<BackendCUDNN> {
    void (*dtor)(void*);

    int (*fwd)(BackendCUDNN& , void*, int);
    int (*bwd)(BackendCUDNN&, void*, int);

    void* desc;
};

std::unordered_map<int, OpDetail<BackendCUDNN>> ops;


struct CUDNNOpDetail {
    void (*dtor)(void*);

    int (*fwd)(cudnnHandle_t , void*, int);
    int (*bwd)(cudnnHandle_t, void*, int);

    void* desc;
};

struct BackendCUDNN {

    template<typename A>
    Operation create_op(A& args, TensorShape& x_dim);

    Tensor fwd(Operation op, Tensor x);

    Tensor backward(Operation op, Tensor dy);
    
    bool destroy_op(Operation op);

    CUDNNOpDetail* get_conv_detail(ConvParam& args, TensorShape& x_dim);

    CUDNNOpDetail* get_linear_detail(LinearParam& args);

    cudnnHandle_t cudnn_handle;
    cublasHandle_t cublas_handle;
    std::unordered_map<int, CUDNNOpDetail*> ops;
};

END_NAMESPACE
