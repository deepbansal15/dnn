#include "backend_cudnn.h"
#include "../tensor_keeper.h"

BEGIN_NAMESPACE

template<>
Operation BackendCUDNN::create_op<ConvParam>(ConvParam& args, TensorShape& x_dim) {}

template<>
Operation BackendCUDNN::create_op<LinearParam>(LinearParam& args, TensorShape& x_dim) {}

Tensor BackendCUDNN::fwd(Operation op, Tensor x) {}

Tensor BackendCUDNN::backward(Operation op, Tensor dy) {}

bool BackendCUDNN::destroy_op(Operation op) {}

CUDNNOpDetail* BackendCUDNN::get_linear_detail(LinearParam& args) {}

END_NAMESPACE