#include "defines.h"
#include "tensor.h"

BEGIN_NAMESPACE

struct TensorKeeper;

template <typename T>
struct Backend {
    void setup(T* b);

    template<typename A>
    Operation create_op(A& args, TensorShape& x_dim, TensorKeeper& keeper);

    Tensor fwd(Operation op, Tensor x);

    Tensor backward(Operation op, Tensor dy);
    
    bool destroy_op(Operation op);

    T* backend_impl;
    TensorKeeper* tensor_keeper;
};

END_NAMESPACE