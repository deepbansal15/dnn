#pragma once

#include "defines.h"

BEGIN_NAMESPACE

/// Note : 1) Only NCHW tensor format is supported for simplicity
///         without losing much in performance  
///        2) Tensor are assumed to be contiguous (FIXME)

struct Tensor { 
    Tensor(int handle) : handle(handle) {}
    int handle;     
};

struct TensorShape { int n, h, w, c; };

/// For now only considering 4d tensors
struct TensorDesc {
    TensorDesc(TensorShape& shape) : shape(shape) {}
    TensorDesc() {}

    TensorShape shape;

    Tensor tensor = -1;
    Tensor parent = -1;

    int byte_size;
	int count;

    float* cpu_data;
    float* gpu_data;
};

struct TensorGradient {
    Tensor t;
    Tensor dt;
};

END_NAMESPACE