#include "tensor_keeper.h"

#include "device.h"
#include "cuda/cuda_utils.h"

#include <cuda_runtime.h>
#include <unordered_map>

BEGIN_NAMESPACE

struct TensorKeeper {
    TensorKeeper():last_handle(0) {}
    std::unordered_map<int, TensorDesc> tensors;
    int last_handle;
};


namespace tensor_keeper {
    TensorKeeper* keeper = nullptr;

	void create()
	{
        if (keeper != nullptr) return;
		keeper = new TensorKeeper();
	}

    TensorDesc& create_descriptor(TensorShape& shape) {
		int size = shape.n * shape.h * shape.w * shape.c;
		
        TensorDesc desc;
		desc.byte_size = size * sizeof(float);
		desc.count = size;

		desc.shape = shape;
		desc.cpu_data = nullptr;
		desc.gpu_data = nullptr;

        desc.tensor = keeper->last_handle;

		keeper->tensors[keeper->last_handle] = desc;
		keeper->last_handle++;

		return keeper->tensors[desc.tensor.handle];
	}

	Tensor create_tensor(Device& device, TensorShape& shape)
	{
		TensorDesc& desc = create_descriptor(shape);
		Tensor tensor = desc.tensor;

		if (device.type == DeviceType::CPU) {
			allocate_cpu_tensor(desc);
		}
		else if (device.type == DeviceType::GPU) {
			allocate_gpu_tensor(desc);
		}

		return tensor;
	}

	void allocate_cpu_tensor(TensorDesc& desc) {
		if(desc.cpu_data == nullptr) 
			desc.cpu_data = (float*)malloc(desc.byte_size);
	}

	void allocate_gpu_tensor(TensorDesc& desc) {
		if (desc.gpu_data == nullptr) {
			check_cuda(cudaMalloc((void**)&desc.gpu_data, desc.byte_size));
		}
	}

	Tensor duplicate_tensor(Tensor tensor, bool copy) {

		/// TODO: A check might be required here
		TensorDesc& olddesc = keeper->tensors[tensor.handle];
        TensorDesc& newdesc = create_descriptor(olddesc.shape);

		if (olddesc.cpu_data != nullptr) {
			allocate_cpu_tensor(newdesc);
			if(copy)
				std::memcpy(newdesc.cpu_data, olddesc.cpu_data, olddesc.byte_size);
		}
				

		if (olddesc.gpu_data != nullptr) {
			allocate_gpu_tensor(newdesc);
			if(copy) {
				check_cuda(cudaMemcpy(newdesc.gpu_data, 
                    olddesc.gpu_data, 
                    olddesc.byte_size, 
                    cudaMemcpyDeviceToDevice));
            }
		}
			
		return newdesc.tensor;
	}

	void destroy_tensor(Tensor tensor) {
		/// Destroy CPU and GPU both the memories
        if (keeper->tensors.find(tensor.handle) != keeper->tensors.end()) {
            TensorDesc desc = keeper->tensors[tensor.handle];
			if (desc.gpu_data != nullptr) {
				/// Delete GPU Data
				check_cuda(cudaFree(desc.gpu_data));
			}

            if (desc.cpu_data != nullptr) {
				/// Delete CPU data
				free(desc.cpu_data);
			}

            keeper->tensors.erase(tensor.handle);
        }
	}

	bool get_descriptor(TensorDesc* desc, Tensor tensor) {
		if (keeper->tensors.find(tensor.handle) != keeper->tensors.end()) {
            desc = &keeper->tensors[tensor.handle];
            return true;
        }
		return false;
	}

	TensorDesc& get_descriptor(Tensor t) {
		TensorDesc* desc = nullptr;
		if (keeper->tensors.find(t.handle) != keeper->tensors.end()) {
            desc = &(keeper->tensors[t.handle]);
        }

		assert(desc);
		return *desc;
	}

	Tensor to_cuda(Tensor tensor) {
		TensorDesc* desc;
        if (get_descriptor(desc, tensor)) {
            allocate_gpu_tensor(*desc);
            check_cuda(cudaMemcpy(desc->gpu_data, desc->cpu_data, 
                desc->byte_size, cudaMemcpyHostToDevice));
        }
		return tensor;
	}

	Tensor to_cpu(Tensor tensor) {
		TensorDesc* desc;
        if (get_descriptor(desc, tensor)) {
            allocate_cpu_tensor(*desc);
            check_cuda(cudaMemcpy(desc->cpu_data, desc->gpu_data, 
                desc->byte_size, cudaMemcpyDeviceToHost));
        }
		return tensor;
	}

	void destroy() {
		/// Destroy all the tensors and their descriptors;
	}

	void get_total_memory(Device* device) {
		/// Return the memory occupied by all the tensors
	}
}

END_NAMESPACE